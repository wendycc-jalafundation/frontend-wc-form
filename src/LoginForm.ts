import { html, css, LitElement } from 'lit';
import { property } from 'lit/decorators.js';

export class LoginForm extends LitElement {
  static styles = css`
    .container{
      background-color: white;

    }
    .form{
      display: flex;
      flex-direction: column;
      gap: 1em;
    }
    .field{
      display: flex;
      flex-direction: column;
      gap: 0.3em;
    }
    button{
      font-family: Verdana, Arial, sans-serif;
      background-color: #1A7DD8;
      padding: 0.5em 1em;
      color: white;
      font-size: larger;
      border-radius: 10px;
      outline: none;
      border-style: none;

    }
    label{
      font-family: Verdana, Arial, sans-serif;
    }
    input{
      border-radius: 10px;
      border-width: 1px;
      border-color: #d4d4d4;
      height: 3em;
      background-color: white
      border-style: solid;
      box-shadow: none;
      outline:none;
      font-family: Verdana, Arial, sans-serif;
    }
    .title{
      font-family: Verdana, Arial, sans-serif;
      font-style: bold;
      font-size: 30px;
      text-align: center
    }
  `;

  @property({ type: String }) title = 'Hey there';

  @property({ type: Number }) counter = 5;

  __increment() {
    this.counter += 1;
  }

  render() {
    return html`
      <div class="container">
        <h3 class="title">Store</h3>
        <div class="form">
          <div class="field">
            <label>Username</label>
            <input placeholder="Enter your email or username" />
          </div>
          <div class="field">
            <label>Password</label>
            <input type="password" placeholder="Enter your password" />
          </div>
          <button>LOGIN</button>
        </div>
      </div>
    `;
  }
}
